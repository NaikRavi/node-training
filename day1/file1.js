// var add = () =>{
//     console.log('Addition done')
// }

// add(() =>{
//     console.log('Inside callback')
// });
// Call back functions are called inside main function after the execution of the main is over
// setTimeout(() =>{
//     return a+b
// },
// 3000)

// add = (a, b) => {
//     setTimeout(() =>{
//         return a+b
//     },
//     3000)
// }

// var result = add(10, 10)

// console.log('Result is',result)

add = (a, b, name) => {
    setTimeout(() =>{
        name(a+b)
    },
    3000)
}

var result = add(10, 10, (ans) =>{
    console.log('Result is ', ans)
})

