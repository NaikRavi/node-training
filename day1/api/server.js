var Express = require('express');
var port = process.env.port || 4000;
var router = require('./routes.js');
var server = Express();
server.use(router);

server.listen(port, ()=>{
    console.log('Server is running');
})