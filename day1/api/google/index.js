var Express = require('express');
var router = Express.Router();
var GoogleController = require('./google.controller.js')

router.get('/exithandler', GoogleController.handleexit)

module.exports = router;