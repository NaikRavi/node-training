var Express = require('express');
var router = Express.Router();
var loginController = require('./logincontroller.js');

router.use('./accenture', require('./accenture'))
router.use('./apple', require('./apple'))
router.use('./tcs',require('./tcs'))
router.use('./google',require('./google'))
// router.post('/signup', loginController.signup)
// router.post('/login', loginController.login)


module.exports = router;