var Express = require('express');
var port = process.env.port || 5000;
var server = Express();
var Fs = require('fs');
var BodyParser = require('body-parser');

server.use((req, es, next)=>{
    console.log('middleware');
    next();
})

server.use(BodyParser.json())

server.get('/',(request, response)=>{
    response.send({
        message:'Base route'
    })
})

server.get('/room27',(request, response)=>{
    console.log('Request query params are', request.query)
    response.send({
        message : 'Hello World',
        user: request.query.user,
        password: request.query.pass
    })
}) 

server.get('/b10',(request, response)=>{
    response.send({
        message: "BDC10"
    })
})

server.get('/file',(request, response)=>{
    var content;
    var filename = './' + request.query.fname;
    console.log(filename)
    Fs.readFile(filename, (err, data)=>{
        console.log(err,data)
        if(err){
            response.send({
                error: 'File not found/Some other error!'
            })
        }
        else{
            content = data.toString();
            response.send({
                message : content
            })
        }
    })
    
})

//************** */Dynamic routing*****************************************
//should be at last

server.get('/download/:file',(req, res)=>{
    filename = './'+req.params.file
    res.download(filename, filename, (err)=>{
        if (err){
            res.send({
                message:'Unable to find the file'
            })
        }
    })
})
// ****************Middleware***********************************************

server.get('/user/:role', (req, res, next)=>{

    if (req.params.role === 'admin'){
        next()
    }
    else{
        res.send({
            message:"you are not admin"
        }
    
        )
    }

}, (req, res)=>{

    res.send({
        message : 'Hello Admin'
    })
})
// *********************************************************************

// Post requests********************************************************
server.post('/login', (req, res)=>{
    console.log('>>>>>>>>>Post<<<<<<<<<<<<')
    console.log('username '+req.body.username)
    console.log('Password '+req.body.password)
    res.send({
        username: 'Hello Ravi'
    })
})
// ************************************************************************
server.get('/:user',(req, res)=>{
    res.send({
        username:req.params.user
    })
})

server.listen(port, ()=>{
    console.log('Server is listening...')
});